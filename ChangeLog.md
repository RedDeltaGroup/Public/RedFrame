# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
* Classes
  * DataFrame
  * Index
  * Series

* Files
  * Arrays
  * DateOffsets
  * Extensions
  * Frequencies
  * General
  * GroupBy
  * Input-Output
  * Plotting
  * Resampling
  * Style
  * Utility
  * Window

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [0.0.2] - 2020-06-03
### Added
* ChangeLog.md
* CONTRIBUTING.md
* LICENSE
* README.md

## [0.0.1] - 2020-06-03
### Added
* .gitignore

[Unreleased]: https://gitlab.com/RedDeltaGroup/Public/RedFrame/-/compare/v0.0.1...master
[0.0.2]: https://gitlab.com/RedDeltaGroup/Public/RedFrame/-/compare/v0.0.1...v0.0.2
[0.0.1]: https://gitlab.com/RedDeltaGroup/Public/RedFrame/-/tags/v0.0.1