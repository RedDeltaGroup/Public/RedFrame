<div style="align-content: center">
  <div style="max-width: 500px;">
    <img alt="" src="https://gitlab.com/RedDeltaGroup/Public/RedFrame/-/raw/master/assets/logo.png">
  </div>
</div>

-----------------

# Red Frame: A PHP Class inspired by Pandas and Numpy
[![Status](https://img.shields.io/badge/status-development-orange)]()
[![License](https://img.shields.io/badge/license-MIT-green)](LICENSE)
[![ChangeLog](https://img.shields.io/badge/ChangeLog-blue)](ChangeLog.md)

## What is it?
**Red Frame** is a PHP Class providing fast, flexible, and expressive data
structures designed to make working with "relational" or "labeled" data both
easy and intuitive. It aims to be the fundamental high-level building block for
doing practical, **real world** data analysis in PHP.

## Main Features
Here are just a few of the things that we hope Red Frame will do:  
* 

## Where to get it
The source code is currently hosted on GitLab at:
https://gitlab.com/RedDeltaGroup/Public/RedFrame

## Dependencies
* None

## License
* [MIT](LICENSE)

## Discussion and Development
Most development discussion is taking place on GitLab in this repo.
