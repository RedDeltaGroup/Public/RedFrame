# Contributing

We welcome your contributions and input on Red Frame. 

You can reach our team by [email](mailto:Info@RedDeltaGroup.com), or submitting an issue or merge request here on GitLab.

Red Delta Group
