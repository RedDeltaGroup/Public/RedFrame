<?php
/** @noinspection PhpUnusedParameterInspection */

/** @noinspection PhpUnused */

namespace RedFrame;


/**
 * Class DataFrame
 *
 * @package RedFrame
 */
class DataFrame
{
   /** Transpose index and columns.
    *
    * @var
    */
   public $T;

   /** Access a single value for a row/column label pair.
    *
    * @var
    */
   public $at;

   /** Dictionary of global attributes on this object.
    *
    * @var
    */
   public $attrs;

   /** Return a list representing the axes of the DataFrame.
    *
    * @var
    */
   public $axes;

   /** The column labels of the DataFrame.
    *
    * @var
    */
   public $columns;

   /** Return the dtypes in the DataFrame.
    *
    * @var
    */
   public $dtypes;

   /** Indicator whether DataFrame is empty.
    *
    * @var
    */
   public $empty;

   /** Access a single value for a row/column pair by integer position.
    *
    * @var
    */
   public $iat;

   /** Purely integer-location based indexing for selection by position.
    *
    * @var
    */
   public $iloc;

   /** The index (row labels) of the DataFrame.
    *
    * @var
    */
   public $index;

   /** Access a group of rows and columns by label(s) or a boolean array.
    *
    * @var
    */
   public $loc;

   /** Return an int representing the number of axes / array dimensions.
    *
    * @var
    */
   public $ndim;

   /** Return a tuple representing the dimensionality of the DataFrame.
    *
    * @var
    */
   public $shape;

   /** Return an int representing the number of elements in this object.
    *
    * @var
    */
   public $size;

   /** Returns a Styler object.
    *
    * @var
    */
   public $style;

   /** Return a Numpy representation of the DataFrame.
    *
    * @var
    */
   public $values;


   /**
    * DataFrame constructor.
    *
    * @param array|null $data
    * @param array|null $index
    * @param array|null $columns
    * @param null       $dtype
    * @param bool       $copy
    */
   public function __construct(
         array $data = null,
         array $index = null,
         array $columns = null,
         $dtype = null,
         $copy = false
   )
   {
   }

   /** Return a Series/DataFrame with absolute numeric value of each element.
    *
    * @param DataFrame $DataFrame
    *
    * @return DataFrame
    */
   public function abs(DataFrame $DataFrame): DataFrame
   {
      return $DataFrame;
   }

   /** Get Addition of dataframe and other, element-wise (binary operator add).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    */
   public function add(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Prefix labels with string prefix.
    *
    * @param DataFrame $DataFrame
    * @param           $prefix
    *
    * @return DataFrame
    */
   public function add_prefix(DataFrame $DataFrame, $prefix): DataFrame
   {
      return $DataFrame;
   }

   /** Suffix labels with string suffix.
    *
    * @param DataFrame $DataFrame
    * @param           $suffix
    *
    * @return DataFrame
    */
   public function add_suffix(DataFrame $DataFrame, $suffix): DataFrame
   {
      return $DataFrame;
   }

   /** Aggregate using one or more operations over the specified axis.
    *
    * @param DataFrame $DataFrame
    * @param           $func
    * @param           $axis
    *
    * @return DataFrame
    */
   public function agg(DataFrame $DataFrame, $func, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Aggregate using one or more operations over the specified axis.
    *
    * @param DataFrame $DataFrame
    * @param           $func
    * @param           $axis
    *
    * @return DataFrame
    */
   public function aggregate(DataFrame $DataFrame, $func, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Align two objects on their axes with the specified join method.
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $join
    * @param           $axis
    * @param           $level
    *
    * @return DataFrame
    */
   public function align(DataFrame $DataFrame, $other, $join, $axis, $level): DataFrame
   {
      return $DataFrame;
   }

   /** Return whether all elements are True, potentially over an axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $bool_only
    * @param           $skipna
    * @param           $level
    *
    * @return DataFrame
    */
   public function all(DataFrame $DataFrame, $axis, $bool_only, $skipna, $level): DataFrame
   {
      return $DataFrame;
   }

   /** Return whether any element is True, potentially over an axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $bool_only
    * @param           $skipna
    * @param           $level
    *
    * @return DataFrame
    */
   public function any(DataFrame $DataFrame, $axis, $bool_only, $skipna, $level): DataFrame
   {
      return $DataFrame;
   }

   /** Append rows of other to the end of caller, returning a new object.
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $ignore_index
    *
    * @return DataFrame
    */
   public function append(DataFrame $DataFrame, $other, $ignore_index): DataFrame
   {
      return $DataFrame;
   }

   /** Apply a function along an axis of the DataFrame.
    *
    * @param DataFrame $DataFrame
    * @param           $func
    * @param           $axis
    * @param           $raw
    * @param           $result_type
    * @param           $args
    *
    * @return DataFrame
    */
   public function apply(DataFrame $DataFrame, $func, $axis, $raw, $result_type, $args): DataFrame
   {
      return $DataFrame;
   }

   /** Apply a function to a Dataframe elementwise.
    *
    * @param DataFrame $DataFrame
    * @param           $func
    *
    * @return DataFrame
    */
   public function applymap(DataFrame $DataFrame, $func): DataFrame
   {
      return $DataFrame;
   }

   /** Convert TimeSeries to specified frequency.
    *
    * @param DataFrame $DataFrame
    * @param           $freq
    * @param           $method
    * @param           $fill_value
    *
    * @return DataFrame
    */
   public function asfreq(DataFrame $DataFrame, $freq, $method, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Return the last row(s) without any NaNs before where.
    *
    * @param DataFrame $DataFrame
    * @param           $where
    * @param           $subset
    *
    * @return DataFrame
    */
   public function asof(DataFrame $DataFrame, $where, $subset): DataFrame
   {
      return $DataFrame;
   }

   /** Assign new columns to a DataFrame.
    *
    * @param DataFrame $DataFrame
    * @param           $kwargs
    *
    * @return DataFrame
    */
   public function assign(DataFrame $DataFrame, $kwargs): DataFrame
   {
      return $DataFrame;
   }

   /** Cast a pandas object to a specified dtype dtype.
    *
    * @param DataFrame $DataFrame
    * @param           $dtype
    * @param           $copy
    * @param           $errors
    *
    * @return DataFrame
    */
   public function astype(DataFrame $DataFrame, $dtype, $copy, $errors): DataFrame
   {
      return $DataFrame;
   }

   /** Select values at particular time of day (e.g.
    *
    * @param DataFrame $DataFrame
    * @param           $time
    * @param           $asof
    * @param           $axis
    *
    * @return DataFrame
    */
   public function at_time(DataFrame $DataFrame, $time, $asof, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Select values between particular times of the day (e.g., 9:00-9:30 AM).
    *
    * @param DataFrame $DataFrame
    * @param           $start_time
    * @param           $end_time
    *
    * @return DataFrame
    */
   public function between_time(DataFrame $DataFrame, $start_time, $end_time): DataFrame
   {
      return $DataFrame;
   }

   /** Synonym for DataFrame.fillna() with method='bfill'.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $limit
    * @param           $downcast
    *
    * @return DataFrame
    */
   public function bfill(DataFrame $DataFrame, $axis, $limit, $downcast): DataFrame
   {
      return $DataFrame;
   }

   /** Return the bool of a single element PandasObject.
    *
    * @param DataFrame $DataFrame
    *
    * @return DataFrame
    */
   public function bool(DataFrame $DataFrame): DataFrame
   {
      return $DataFrame;
   }

   /** Make a box plot from DataFrame columns.
    *
    * @param DataFrame $DataFrame
    * @param           $column
    * @param           $by
    * @param           $ax
    * @param           $fontsize
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function boxplot(DataFrame $DataFrame, $column, $by, $ax, $fontsize): DataFrame
   {
      return $DataFrame;
   }

   /** Trim values at input threshold(s).
    *
    * @param DataFrame $DataFrame
    * @param           $lower
    * @param           $upper
    * @param           $axis
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function clip(DataFrame $DataFrame, $lower, $upper, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Perform column-wise combine with another DataFrame.
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $func
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function combine(DataFrame $DataFrame, $other, $func, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Update null elements with value in the same location in other.
    *
    * @param DataFrame $DataFrame
    * @param           $other
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function combine_first(DataFrame $DataFrame, $other): DataFrame
   {
      return $DataFrame;
   }

   /** Convert columns to best possible dtypes using dtypes supporting pd.NA.
    *
    * @param DataFrame $DataFrame
    * @param           $infer_objects
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function convert_dtypes(DataFrame $DataFrame, $infer_objects): DataFrame
   {
      return $DataFrame;
   }

   /** Make a copy of this object’s indices and data.
    *
    * @param DataFrame $DataFrame
    * @param           $deep
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function copy(DataFrame $DataFrame, $deep): DataFrame
   {
      return $DataFrame;
   }

   /** Compute pairwise correlation of columns, excluding NA/null values.
    *
    * @param DataFrame $DataFrame
    * @param           $method
    * @param           $min_periods
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function corr(DataFrame $DataFrame, $method, $min_periods): DataFrame
   {
      return $DataFrame;
   }

   /** Compute pairwise correlation.
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $drop
    * @param           $method
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function corrwith(DataFrame $DataFrame, $other, $axis, $drop, $method): DataFrame
   {
      return $DataFrame;
   }

   /** Count non-NA cells for each column or row.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $level
    * @param           $numeric_only
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function count(DataFrame $DataFrame, $axis, $level, $numeric_only): DataFrame
   {
      return $DataFrame;
   }

   /** Compute pairwise covariance of columns, excluding NA/null values.
    *
    * @param DataFrame $DataFrame
    * @param           $min_periods
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function cov(DataFrame $DataFrame, $min_periods): DataFrame
   {
      return $DataFrame;
   }

   /** Return cumulative maximum over a DataFrame or Series axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function cummax(DataFrame $DataFrame, $axis, $skipna): DataFrame
   {
      return $DataFrame;
   }

   /** Return cumulative minimum over a DataFrame or Series axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function cummin(DataFrame $DataFrame, $axis, $skipna): DataFrame
   {
      return $DataFrame;
   }

   /** Return cumulative product over a DataFrame or Series axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function cumprod(DataFrame $DataFrame, $axis, $skipna): DataFrame
   {
      return $DataFrame;
   }

   /** Return cumulative sum over a DataFrame or Series axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function cumsum(DataFrame $DataFrame, $axis, $skipna): DataFrame
   {
      return $DataFrame;
   }

   /** Generate descriptive statistics.
    *
    * @param DataFrame $DataFrame
    * @param           $percentiles
    * @param           $include
    * @param           $exclude
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function describe(DataFrame $DataFrame, $percentiles, $include, $exclude): DataFrame
   {
      return $DataFrame;
   }

   /** First discrete difference of element.
    *
    * @param DataFrame $DataFrame
    * @param           $periods
    * @param           $axis
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function diff(DataFrame $DataFrame, $periods, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Get Floating division of dataframe and other, element-wise (binary operator truediv).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function div(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Get Floating division of dataframe and other, element-wise (binary operator truediv).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function divide(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Compute the matrix multiplication between the DataFrame and other.
    *
    * @param DataFrame $DataFrame
    * @param           $other
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function dot(DataFrame $DataFrame, $other): DataFrame
   {
      return $DataFrame;
   }

   /** Drop specified labels from rows or columns.
    *
    * @param DataFrame $DataFrame
    * @param           $labels
    * @param           $axis
    * @param           $index
    * @param           $columns
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function drop(DataFrame $DataFrame, $labels, $axis, $index, $columns): DataFrame
   {
      return $DataFrame;
   }

   /** Return DataFrame with duplicate rows removed.
    *
    * @param DataFrame $DataFrame
    * @param           $subset
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function drop_duplicates(DataFrame $DataFrame, $subset): DataFrame
   {
      return $DataFrame;
   }

   /** Return DataFrame with requested index / column level(s) removed.
    *
    * @param DataFrame $DataFrame
    * @param           $level
    * @param           $axis
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function droplevel(DataFrame $DataFrame, $level, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Remove missing values.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $how
    * @param           $thresh
    * @param           $subset
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function dropna(DataFrame $DataFrame, $axis, $how, $thresh, $subset): DataFrame
   {
      return $DataFrame;
   }

   /** Return boolean Series denoting duplicate rows.
    *
    * @param DataFrame $DataFrame
    * @param           $subset
    * @param           $SequenceHashable
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function duplicated(DataFrame $DataFrame, $subset, $SequenceHashable): DataFrame
   {
      return $DataFrame;
   }

   /** Get Equal to of dataframe and other, element-wise (binary operator eq).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function eq(DataFrame $DataFrame, $other, $axis, $level): DataFrame
   {
      return $DataFrame;
   }

   /** Test whether two objects contain the same elements.
    *
    * @param DataFrame $DataFrame
    * @param           $other
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function equals(DataFrame $DataFrame, $other): DataFrame
   {
      return $DataFrame;
   }

   /** Evaluate a string describing operations on DataFrame columns.
    *
    * @param DataFrame $DataFrame
    * @param           $expr
    * @param           $inplace
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function eval(DataFrame $DataFrame, $expr, $inplace): DataFrame
   {
      return $DataFrame;
   }

   /** Provide exponential weighted functions.
    *
    * @param DataFrame $DataFrame
    * @param           $com
    * @param           $span
    * @param           $halflife
    * @param           $alpha
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function ewm(DataFrame $DataFrame, $com, $span, $halflife, $alpha): DataFrame
   {
      return $DataFrame;
   }

   /** Provide expanding transformations.
    *
    * @param DataFrame $DataFrame
    * @param           $min_periods
    * @param           $center
    * @param           $axis
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function expanding(DataFrame $DataFrame, $min_periods, $center, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Transform each element of a list-like to a row, replicating index values.
    *
    * @param DataFrame $DataFrame
    * @param           $column
    * @param           $Tuple
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function explode(DataFrame $DataFrame, $column, $Tuple): DataFrame
   {
      return $DataFrame;
   }

   /** Synonym for DataFrame.fillna() with method='ffill'.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $limit
    * @param           $downcast
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function ffill(DataFrame $DataFrame, $axis, $limit, $downcast): DataFrame
   {
      return $DataFrame;
   }

   /** Fill NA/NaN values using the specified method.
    *
    * @param DataFrame $DataFrame
    * @param           $value
    * @param           $method
    * @param           $axis
    * @param           $inplace
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function fillna(DataFrame $DataFrame, $value, $method, $axis, $inplace): DataFrame
   {
      return $DataFrame;
   }

   /** Subset the dataframe rows or columns according to the specified index labels.
    *
    * @param DataFrame $DataFrame
    * @param           $items
    * @param           $axis
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function filter(DataFrame $DataFrame, $items, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Method to subset initial periods of time series data based on a date offset.
    *
    * @param DataFrame $DataFrame
    * @param           $offset
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function first(DataFrame $DataFrame, $offset): DataFrame
   {
      return $DataFrame;
   }

   /** Return index for first non-NA/null value.
    *
    * @param DataFrame $DataFrame
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function first_valid_index(DataFrame $DataFrame): DataFrame
   {
      return $DataFrame;
   }

   /** Get Integer division of dataframe and other, element-wise (binary operator floordiv).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function floordiv(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Construct DataFrame from dict of array-like or dicts.
    *
    * @param array $data
    * @param       $orient
    * @param       $dtype
    * @param       $columns
    *
    * @return array
    */
   public function from_dict(array $data, $orient, $dtype, $columns): array
   {
      return $data;
   }

   /** Convert structured or record ndarray to DataFrame.
    *
    * @param array $data
    * @param       $index
    * @param       $exclude
    *
    * @return array
    */
   public function from_records(array $data, $index, $exclude): array
   {
      return $data;
   }

   /** Get Greater than or equal to of dataframe and other, element-wise (binary operator ge).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function ge(DataFrame $DataFrame, $other, $axis, $level): DataFrame
   {
      return $DataFrame;
   }

   /** Get item from object for given key (ex: DataFrame column).
    *
    * @param DataFrame $DataFrame
    * @param           $key
    * @param           $default
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function get(DataFrame $DataFrame, $key, $default): DataFrame
   {
      return $DataFrame;
   }

   /** Group DataFrame using a mapper or by a Series of columns.
    *
    * @param DataFrame $DataFrame
    * @param           $by
    * @param           $axis
    * @param           $level
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function groupby(DataFrame $DataFrame, $by, $axis, $level): DataFrame
   {
      return $DataFrame;
   }

   /** Get Greater than of dataframe and other, element-wise (binary operator gt).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function gt(DataFrame $DataFrame, $other, $axis, $level): DataFrame
   {
      return $DataFrame;
   }

   /** Return the first n rows.
    *
    * @param DataFrame $DataFrame
    * @param           $n
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function head(DataFrame $DataFrame, $n): DataFrame
   {
      return $DataFrame;
   }

   /** Make a histogram of the DataFrame’s.
    *
    * @param array $data
    * @param       $column
    * @param       $by
    * @param       $grid
    * @param       $xlabelsize
    *
    * @return array
    */
   public function hist(array $data, $column, $by, $grid, $xlabelsize): array
   {
      return $data;
   }

   /** Return index of first occurrence of maximum over requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function idxmax(DataFrame $DataFrame, $axis, $skipna): DataFrame
   {
      return $DataFrame;
   }

   /** Return index of first occurrence of minimum over requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function idxmin(DataFrame $DataFrame, $axis, $skipna): DataFrame
   {
      return $DataFrame;
   }

   /** Attempt to infer better dtypes for object columns.
    *
    * @param DataFrame $DataFrame
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function infer_objects(DataFrame $DataFrame): DataFrame
   {
      return $DataFrame;
   }

   /** Print a concise summary of a DataFrame.
    *
    * @param DataFrame $DataFrame
    * @param           $verbose
    * @param           $buf
    * @param           $max_cols
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function info(DataFrame $DataFrame, $verbose, $buf, $max_cols): DataFrame
   {
      return $DataFrame;
   }

   /** Insert column into DataFrame at specified location.
    *
    * @param DataFrame $DataFrame
    * @param           $loc
    * @param           $column
    * @param           $value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function insert(DataFrame $DataFrame, $loc, $column, $value): DataFrame
   {
      return $DataFrame;
   }

   /** Interpolate values according to different methods.
    *
    * @param DataFrame $DataFrame
    * @param           $method
    * @param           $axis
    * @param           $limit
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function interpolate(DataFrame $DataFrame, $method, $axis, $limit): DataFrame
   {
      return $DataFrame;
   }

   /** Whether each element in the DataFrame is contained in values.
    *
    * @param DataFrame $DataFrame
    * @param           $values
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function isin(DataFrame $DataFrame, $values): DataFrame
   {
      return $DataFrame;
   }

   /** Detect missing values.
    *
    * @param DataFrame $DataFrame
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function isna(DataFrame $DataFrame): DataFrame
   {
      return $DataFrame;
   }

   /** Detect missing values.
    *
    * @param DataFrame $DataFrame
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function isnull(DataFrame $DataFrame): DataFrame
   {
      return $DataFrame;
   }

   /** Iterate over (column name, Series) pairs.
    *
    * @param DataFrame $DataFrame
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function items(DataFrame $DataFrame): DataFrame
   {
      return $DataFrame;
   }

   /** Iterate over (column name, Series) pairs.
    *
    * @param DataFrame $DataFrame
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function iteritems(DataFrame $DataFrame): DataFrame
   {
      return $DataFrame;
   }

   /** Iterate over DataFrame rows as (index, Series) pairs.
    *
    * @param DataFrame $DataFrame
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function iterrows(DataFrame $DataFrame): DataFrame
   {
      return $DataFrame;
   }

   /** Iterate over DataFrame rows as namedtuples.
    *
    * @param DataFrame $DataFrame
    * @param           $index
    * @param           $name
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function itertuples(DataFrame $DataFrame, $index, $name): DataFrame
   {
      return $DataFrame;
   }

   /** Join columns of another DataFrame.
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $on
    * @param           $how
    * @param           $lsuffix
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function join(DataFrame $DataFrame, $other, $on, $how, $lsuffix): DataFrame
   {
      return $DataFrame;
   }

   /** Get the ‘info axis’ (see Indexing for more).
    *
    * @param DataFrame $DataFrame
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function keys(DataFrame $DataFrame): DataFrame
   {
      return $DataFrame;
   }

   /** Return unbiased kurtosis over requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    * @param           $level
    * @param           $numeric_only
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function kurt(DataFrame $DataFrame, $axis, $skipna, $level, $numeric_only): DataFrame
   {
      return $DataFrame;
   }

   /** Return unbiased kurtosis over requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    * @param           $level
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function kurtosis(DataFrame $DataFrame, $axis, $skipna, $level): DataFrame
   {
      return $DataFrame;
   }

   /** Method to subset final periods of time series data based on a date offset.
    *
    * @param DataFrame $DataFrame
    * @param           $offset
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function last(DataFrame $DataFrame, $offset): DataFrame
   {
      return $DataFrame;
   }

   /** Return index for last non-NA/null value.
    *
    * @param DataFrame $DataFrame
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function last_valid_index(DataFrame $DataFrame): DataFrame
   {
      return $DataFrame;
   }

   /** Get Less than or equal to of dataframe and other, element-wise (binary operator le).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function le(DataFrame $DataFrame, $other, $axis, $level): DataFrame
   {
      return $DataFrame;
   }

   /** Label-based “fancy indexing” function for DataFrame.
    *
    * @param DataFrame $DataFrame
    * @param           $row_labels
    * @param           $col_labels
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function lookup(DataFrame $DataFrame, $row_labels, $col_labels): DataFrame
   {
      return $DataFrame;
   }

   /** Get Less than of dataframe and other, element-wise (binary operator lt).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function lt(DataFrame $DataFrame, $other, $axis, $level): DataFrame
   {
      return $DataFrame;
   }

   /** Return the mean absolute deviation of the values for the requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    * @param           $level
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function mad(DataFrame $DataFrame, $axis, $skipna, $level): DataFrame
   {
      return $DataFrame;
   }

   /** Replace values where the condition is True.
    *
    * @param DataFrame $DataFrame
    * @param           $cond
    * @param           $other
    * @param           $inplace
    * @param           $axis
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function mask(DataFrame $DataFrame, $cond, $other, $inplace, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Return the maximum of the values for the requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    * @param           $level
    * @param           $numeric_only
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function max(DataFrame $DataFrame, $axis, $skipna, $level, $numeric_only): DataFrame
   {
      return $DataFrame;
   }

   /** Return the mean of the values for the requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    * @param           $level
    * @param           $numeric_only
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function mean(DataFrame $DataFrame, $axis, $skipna, $level, $numeric_only): DataFrame
   {
      return $DataFrame;
   }

   /** Return the median of the values for the requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    * @param           $level
    * @param           $numeric_only
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function median(DataFrame $DataFrame, $axis, $skipna, $level, $numeric_only): DataFrame
   {
      return $DataFrame;
   }

   /** Unpivot a DataFrame from wide to long format, optionally leaving identifiers set.
    *
    * @param DataFrame $DataFrame
    * @param           $id_vars
    * @param           $value_vars
    * @param           $var_name
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function melt(DataFrame $DataFrame, $id_vars, $value_vars, $var_name): DataFrame
   {
      return $DataFrame;
   }

   /** Return the memory usage of each column in bytes.
    *
    * @param DataFrame $DataFrame
    * @param           $index
    * @param           $deep
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function memory_usage(DataFrame $DataFrame, $index, $deep): DataFrame
   {
      return $DataFrame;
   }

   /** Merge DataFrame or named Series objects with a database-style join.
    *
    * @param DataFrame $DataFrame
    * @param           $right
    * @param           $how
    * @param           $on
    * @param           $left_on
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function merge(DataFrame $DataFrame, $right, $how, $on, $left_on): DataFrame
   {
      return $DataFrame;
   }

   /** Return the minimum of the values for the requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    * @param           $level
    * @param           $numeric_only
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function min(DataFrame $DataFrame, $axis, $skipna, $level, $numeric_only): DataFrame
   {
      return $DataFrame;
   }

   /** Get Modulo of dataframe and other, element-wise (binary operator mod).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function mod(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Get the mode(s) of each element along the selected axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $numeric_only
    * @param           $dropna
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function mode(DataFrame $DataFrame, $axis, $numeric_only, $dropna): DataFrame
   {
      return $DataFrame;
   }

   /** Get Multiplication of dataframe and other, element-wise (binary operator mul).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function mul(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Get Multiplication of dataframe and other, element-wise (binary operator mul).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function multiply(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Get Not equal to of dataframe and other, element-wise (binary operator ne).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function ne(DataFrame $DataFrame, $other, $axis, $level): DataFrame
   {
      return $DataFrame;
   }

   /** Return the first n rows ordered by columns in descending order.
    *
    * @param DataFrame $DataFrame
    * @param           $n
    * @param           $columns
    * @param           $keep
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function nlargest(DataFrame $DataFrame, $n, $columns, $keep): DataFrame
   {
      return $DataFrame;
   }

   /** Detect existing (non-missing) values.
    *
    * @param DataFrame $DataFrame
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function notna(DataFrame $DataFrame): DataFrame
   {
      return $DataFrame;
   }

   /** Detect existing (non-missing) values.
    *
    * @param DataFrame $DataFrame
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function notnull(DataFrame $DataFrame): DataFrame
   {
      return $DataFrame;
   }

   /** Return the first n rows ordered by columns in ascending order.
    *
    * @param DataFrame $DataFrame
    * @param           $n
    * @param           $columns
    * @param           $keep
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function nsmallest(DataFrame $DataFrame, $n, $columns, $keep): DataFrame
   {
      return $DataFrame;
   }

   /** Count distinct observations over requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $dropna
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function nunique(DataFrame $DataFrame, $axis, $dropna): DataFrame
   {
      return $DataFrame;
   }

   /** Percentage change between the current and a prior element.
    *
    * @param DataFrame $DataFrame
    * @param           $periods
    * @param           $fill_method
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function pct_change(DataFrame $DataFrame, $periods, $fill_method): DataFrame
   {
      return $DataFrame;
   }

   /** Apply func(self, *args, **kwargs).
    *
    * @param DataFrame $DataFrame
    * @param           $func
    * @param           $args
    * @param           $kwargs
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function pipe(DataFrame $DataFrame, $func, $args, $kwargs): DataFrame
   {
      return $DataFrame;
   }

   /** Return reshaped DataFrame organized by given index / column values.
    *
    * @param DataFrame $DataFrame
    * @param           $index
    * @param           $columns
    * @param           $values
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function pivot(DataFrame $DataFrame, $index, $columns, $values): DataFrame
   {
      return $DataFrame;
   }

   /** Create a spreadsheet-style pivot table as a DataFrame.
    *
    * @param DataFrame $DataFrame
    * @param           $values
    * @param           $index
    * @param           $columns
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function pivot_table(DataFrame $DataFrame, $values, $index, $columns): DataFrame
   {
      return $DataFrame;
   }

   /** alias of pandas.plotting._core.PlotAccessor**/
   public function plot(): DataFrame
   {
      return new DataFrame();
   }

   /** Return item and drop from frame.
    *
    * @param DataFrame $DataFrame
    * @param           $item
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function pop(DataFrame $DataFrame, $item): DataFrame
   {
      return $DataFrame;
   }

   /** Get Exponential power of dataframe and other, element-wise (binary operator pow).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function pow(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Return the product of the values for the requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    * @param           $level
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function prod(DataFrame $DataFrame, $axis, $skipna, $level): DataFrame
   {
      return $DataFrame;
   }

   /** Return the product of the values for the requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    * @param           $level
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function product(DataFrame $DataFrame, $axis, $skipna, $level): DataFrame
   {
      return $DataFrame;
   }

   /** Return values at the given quantile over requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $q
    * @param           $axis
    * @param           $numeric_only
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function quantile(DataFrame $DataFrame, $q, $axis, $numeric_only): DataFrame
   {
      return $DataFrame;
   }

   /** Query the columns of a DataFrame with a boolean expression.
    *
    * @param DataFrame $DataFrame
    * @param           $expr
    * @param           $inplace
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function query(DataFrame $DataFrame, $expr, $inplace): DataFrame
   {
      return $DataFrame;
   }

   /** Get Addition of dataframe and other, element-wise (binary operator radd).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function radd(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Compute numerical data ranks (1 through n) along axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function rank(DataFrame $DataFrame, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Get Floating division of dataframe and other, element-wise (binary operator rtruediv).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function rdiv(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Conform DataFrame to new index with optional filling logic.
    *
    * @param DataFrame $DataFrame
    * @param           $labels
    * @param           $index
    * @param           $columns
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function reindex(DataFrame $DataFrame, $labels, $index, $columns): DataFrame
   {
      return $DataFrame;
   }

   /** Return an object with matching indices as other object.
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $method
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function reindex_like(DataFrame $DataFrame, $other, $method): DataFrame
   {
      return $DataFrame;
   }

   /** Alter axes labels.
    *
    * @param DataFrame $DataFrame
    * @param           $mapper
    * @param           $index
    * @param           $columns
    * @param           $axis
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function rename(DataFrame $DataFrame, $mapper, $index, $columns, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Set the name of the axis for the index or columns.
    *
    * @param DataFrame $DataFrame
    * @param           $mapper
    * @param           $index
    * @param           $columns
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function rename_axis(DataFrame $DataFrame, $mapper, $index, $columns): DataFrame
   {
      return $DataFrame;
   }

   /** Rearrange index levels using input order.
    *
    * @param DataFrame $DataFrame
    * @param           $order
    * @param           $axis
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function reorder_levels(DataFrame $DataFrame, $order, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Replace values given in to_replace with value.
    *
    * @param DataFrame $DataFrame
    * @param           $to_replace
    * @param           $value
    * @param           $inplace
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function replace(DataFrame $DataFrame, $to_replace, $value, $inplace): DataFrame
   {
      return $DataFrame;
   }

   /** Resample time-series data.
    *
    * @param DataFrame $DataFrame
    * @param           $rule
    * @param           $axis
    * @param           $loffset
    * @param           $on
    * @param           $level
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function resample(DataFrame $DataFrame, $rule, $axis, $loffset, $on, $level): DataFrame
   {
      return $DataFrame;
   }

   /** Reset the index, or a level of it.
    *
    * @param DataFrame $DataFrame
    * @param           $level
    * @param           $SequenceHashable
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function reset_index(DataFrame $DataFrame, $level, $SequenceHashable): DataFrame
   {
      return $DataFrame;
   }

   /** Get Integer division of dataframe and other, element-wise (binary operator rfloordiv).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function rfloordiv(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Get Modulo of dataframe and other, element-wise (binary operator rmod).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function rmod(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Get Multiplication of dataframe and other, element-wise (binary operator rmul).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function rmul(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Provide rolling window calculations.
    *
    * @param DataFrame $DataFrame
    * @param           $window
    * @param           $min_periods
    * @param           $center
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function rolling(DataFrame $DataFrame, $window, $min_periods, $center): DataFrame
   {
      return $DataFrame;
   }

   /** Round a DataFrame to a variable number of decimal places.
    *
    * @param DataFrame $DataFrame
    * @param           $decimals
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function round(DataFrame $DataFrame, $decimals): DataFrame
   {
      return $DataFrame;
   }

   /** Get Exponential power of dataframe and other, element-wise (binary operator rpow).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function rpow(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Get Subtraction of dataframe and other, element-wise (binary operator rsub).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function rsub(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Get Floating division of dataframe and other, element-wise (binary operator rtruediv).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function rtruediv(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Return a random sample of items from an axis of object.
    *
    * @param DataFrame $DataFrame
    * @param           $n
    * @param           $frac
    * @param           $replace
    * @param           $weights
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function sample(DataFrame $DataFrame, $n, $frac, $replace, $weights): DataFrame
   {
      return $DataFrame;
   }

   /** Return a subset of the DataFrame’s columns based on the column dtypes.
    *
    * @param DataFrame $DataFrame
    * @param           $include
    * @param           $exclude
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function select_dtypes(DataFrame $DataFrame, $include, $exclude): DataFrame
   {
      return $DataFrame;
   }

   /** Return unbiased standard error of the mean over requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    * @param           $level
    * @param           $ddof
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function sem(DataFrame $DataFrame, $axis, $skipna, $level, $ddof): DataFrame
   {
      return $DataFrame;
   }

   /** Assign desired index to given axis.
    *
    * @param DataFrame $DataFrame
    * @param           $labels
    * @param           $axis
    * @param           $inplace
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function set_axis(DataFrame $DataFrame, $labels, $axis, $inplace): DataFrame
   {
      return $DataFrame;
   }

   /** Set the DataFrame index using existing columns.
    *
    * @param DataFrame $DataFrame
    * @param           $keys
    * @param           $drop
    * @param           $append
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function set_index(DataFrame $DataFrame, $keys, $drop, $append): DataFrame
   {
      return $DataFrame;
   }

   /** Shift index by desired number of periods with an optional time freq.
    *
    * @param DataFrame $DataFrame
    * @param           $periods
    * @param           $freq
    * @param           $axis
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function shift(DataFrame $DataFrame, $periods, $freq, $axis, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Return unbiased skew over requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    * @param           $level
    * @param           $numeric_only
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function skew(DataFrame $DataFrame, $axis, $skipna, $level, $numeric_only): DataFrame
   {
      return $DataFrame;
   }

   /** Equivalent to shift without copying data.
    *
    * @param DataFrame $DataFrame
    * @param           $periods
    * @param           $axis
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function slice_shift(DataFrame $DataFrame, $periods, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Sort object by labels (along an axis).
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $level
    * @param           $ascending
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function sort_index(DataFrame $DataFrame, $axis, $level, $ascending): DataFrame
   {
      return $DataFrame;
   }

   /** Sort by the values along either axis.
    *
    * @param DataFrame $DataFrame
    * @param           $by
    * @param           $axis
    * @param           $ascending
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function sort_values(DataFrame $DataFrame, $by, $axis, $ascending): DataFrame
   {
      return $DataFrame;
   }

   /** alias of pandas.core.arrays.sparse.accessor.SparseFrameAccessor**/
   public function sparse(): DataFrame
   {
      return new DataFrame();
   }

   /** Squeeze 1 dimensional axis objects into scalars.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function squeeze(DataFrame $DataFrame, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Stack the prescribed level(s) from columns to index.
    *
    * @param DataFrame $DataFrame
    * @param           $level
    * @param           $dropna
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function stack(DataFrame $DataFrame, $level, $dropna): DataFrame
   {
      return $DataFrame;
   }

   /** Return sample standard deviation over requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    * @param           $level
    * @param           $ddof
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function std(DataFrame $DataFrame, $axis, $skipna, $level, $ddof): DataFrame
   {
      return $DataFrame;
   }

   /** Get Subtraction of dataframe and other, element-wise (binary operator sub).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function sub(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Get Subtraction of dataframe and other, element-wise (binary operator sub).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function subtract(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Return the sum of the values for the requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    * @param           $level
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function sum(DataFrame $DataFrame, $axis, $skipna, $level): DataFrame
   {
      return $DataFrame;
   }

   /** Interchange axes and swap values axes appropriately.
    *
    * @param DataFrame $DataFrame
    * @param           $axis1
    * @param           $axis2
    * @param           $copy
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function swapaxes(DataFrame $DataFrame, $axis1, $axis2, $copy): DataFrame
   {
      return $DataFrame;
   }

   /** Swap levels i and j in a MultiIndex on a particular axis.
    *
    * @param DataFrame $DataFrame
    * @param           $i
    * @param           $j
    * @param           $axis
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function swaplevel(DataFrame $DataFrame, $i, $j, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Return the last n rows.
    *
    * @param DataFrame $DataFrame
    * @param           $n
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function tail(DataFrame $DataFrame, $n): DataFrame
   {
      return $DataFrame;
   }

   /** Return the elements in the given positional indices along an axis.
    *
    * @param DataFrame $DataFrame
    * @param           $indices
    * @param           $axis
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function take(DataFrame $DataFrame, $indices, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Copy object to the system clipboard.
    *
    * @param DataFrame $DataFrame
    * @param           $excel
    * @param           $sep
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_clipboard(DataFrame $DataFrame, $excel, $sep): DataFrame
   {
      return $DataFrame;
   }

   /** Write object to a comma-separated values (csv) file.
    *
    * @param DataFrame $DataFrame
    * @param           $path_or_buf
    * @param           $path
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_csv(DataFrame $DataFrame, $path_or_buf, $path): DataFrame
   {
      return $DataFrame;
   }

   /** Convert the DataFrame to a dictionary.
    *
    * @param DataFrame $DataFrame
    * @param           $orient
    * @param           $into
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_dict(DataFrame $DataFrame, $orient, $into): DataFrame
   {
      return $DataFrame;
   }

   /** Write object to an Excel sheet.
    *
    * @param DataFrame $DataFrame
    * @param           $excel_writer
    * @param           $sheet_name
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_excel(DataFrame $DataFrame, $excel_writer, $sheet_name): DataFrame
   {
      return $DataFrame;
   }

   /** Write out the binary feather-format for DataFrames.
    *
    * @param DataFrame $DataFrame
    * @param           $path
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_feather(DataFrame $DataFrame, $path): DataFrame
   {
      return $DataFrame;
   }

   /** Write a DataFrame to a Google BigQuery table.
    *
    * @param DataFrame $DataFrame
    * @param           $destination_table
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_gbq(DataFrame $DataFrame, $destination_table): DataFrame
   {
      return $DataFrame;
   }

   /** Write the contained data to an HDF5 file using HDFStore.
    *
    * @param DataFrame $DataFrame
    * @param           $path_or_buf
    * @param           $key
    * @param           $mode
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_hdf(DataFrame $DataFrame, $path_or_buf, $key, $mode): DataFrame
   {
      return $DataFrame;
   }

   /** Render a DataFrame as an HTML table.
    *
    * @param DataFrame $DataFrame
    * @param           $buf
    * @param           $columns
    * @param           $col_space
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_html(DataFrame $DataFrame, $buf, $columns, $col_space): DataFrame
   {
      return $DataFrame;
   }

   /** Convert the object to a JSON string.
    *
    * @param DataFrame $DataFrame
    * @param           $path_or_buf
    * @param           $path
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_json(DataFrame $DataFrame, $path_or_buf, $path): DataFrame
   {
      return $DataFrame;
   }

   /** Render object to a LaTeX tabular, longtable, or nested table/tabular.
    *
    * @param DataFrame $DataFrame
    * @param           $buf
    * @param           $columns
    * @param           $col_space
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_latex(DataFrame $DataFrame, $buf, $columns, $col_space): DataFrame
   {
      return $DataFrame;
   }

   /** Print DataFrame in Markdown-friendly format.
    *
    * @param DataFrame $DataFrame
    * @param           $buf
    * @param           $NoneType
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_markdown(DataFrame $DataFrame, $buf, $NoneType = null): DataFrame
   {
      return $DataFrame;
   }

   /** Convert the DataFrame to a NumPy array.
    *
    * @param DataFrame $DataFrame
    * @param           $dtype
    * @param           $copy
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_numpy(DataFrame $DataFrame, $dtype, $copy): DataFrame
   {
      return $DataFrame;
   }

   /** Write a DataFrame to the binary parquet format.
    *
    * @param DataFrame $DataFrame
    * @param           $path
    * @param           $engine
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_parquet(DataFrame $DataFrame, $path, $engine): DataFrame
   {
      return $DataFrame;
   }

   /** Convert DataFrame from DatetimeIndex to PeriodIndex.
    *
    * @param DataFrame $DataFrame
    * @param           $freq
    * @param           $axis
    * @param           $copy
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_period(DataFrame $DataFrame, $freq, $axis, $copy): DataFrame
   {
      return $DataFrame;
   }

   /** Pickle (serialize) object to file.
    *
    * @param DataFrame $DataFrame
    * @param           $path
    * @param           $compression
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_pickle(DataFrame $DataFrame, $path, $compression): DataFrame
   {
      return $DataFrame;
   }

   /** Convert DataFrame to a NumPy record array.
    *
    * @param DataFrame $DataFrame
    * @param           $index
    * @param           $column_dtypes
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_records(DataFrame $DataFrame, $index, $column_dtypes): DataFrame
   {
      return $DataFrame;
   }

   /** Write records stored in a DataFrame to a SQL database.
    *
    * @param DataFrame $DataFrame
    * @param           $name
    * @param           $con
    * @param           $schema
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_sql(DataFrame $DataFrame, $name, $con, $schema): DataFrame
   {
      return $DataFrame;
   }

   /** Export DataFrame object to Stata dta format.
    *
    * @param DataFrame $DataFrame
    * @param           $path
    * @param           $convert_dates
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_stata(DataFrame $DataFrame, $path, $convert_dates): DataFrame
   {
      return $DataFrame;
   }

   /** Render a DataFrame to a console-friendly tabular output.
    *
    * @param DataFrame $DataFrame
    * @param           $buf
    * @param           $path
    * @param           $IOstr
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_string(DataFrame $DataFrame, $buf, $path, $IOstr): DataFrame
   {
      return $DataFrame;
   }

   /** Cast to DatetimeIndex of timestamps, at beginning of period.
    *
    * @param DataFrame $DataFrame
    * @param           $freq
    * @param           $how
    * @param           $axis
    * @param           $copy
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_timestamp(DataFrame $DataFrame, $freq, $how, $axis, $copy): DataFrame
   {
      return $DataFrame;
   }

   /** Return an xarray object from the pandas object.
    *
    * @param DataFrame $DataFrame
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function to_xarray(DataFrame $DataFrame): DataFrame
   {
      return $DataFrame;
   }

   /** Call func on self producing a DataFrame with transformed values.
    *
    * @param DataFrame $DataFrame
    * @param           $func
    * @param           $axis
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function transform(DataFrame $DataFrame, $func, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Transpose index and columns.
    *
    * @param DataFrame $DataFrame
    * @param           $args
    * @param           $copy
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function transpose(DataFrame $DataFrame, $args, $copy): DataFrame
   {
      return $DataFrame;
   }

   /** Get Floating division of dataframe and other, element-wise (binary operator truediv).
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $axis
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function truediv(DataFrame $DataFrame, $other, $axis, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Truncate a Series or DataFrame before and after some index value.
    *
    * @param DataFrame $DataFrame
    * @param           $before
    * @param           $after
    * @param           $axis
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function truncate(DataFrame $DataFrame, $before, $after, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Shift the time index, using the index’s frequency if available.
    *
    * @param DataFrame $DataFrame
    * @param           $periods
    * @param           $freq
    * @param           $axis
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function tshift(DataFrame $DataFrame, $periods, $freq, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Convert tz-aware axis to target time zone.
    *
    * @param DataFrame $DataFrame
    * @param           $tz
    * @param           $axis
    * @param           $level
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function tz_convert(DataFrame $DataFrame, $tz, $axis, $level): DataFrame
   {
      return $DataFrame;
   }

   /** Localize tz-naive index of a Series or DataFrame to target time zone.
    *
    * @param DataFrame $DataFrame
    * @param           $tz
    * @param           $axis
    * @param           $level
    * @param           $ambiguous
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function tz_localize(DataFrame $DataFrame, $tz, $axis, $level, $ambiguous): DataFrame
   {
      return $DataFrame;
   }

   /** Pivot a level of the (necessarily hierarchical) index labels.
    *
    * @param DataFrame $DataFrame
    * @param           $level
    * @param           $fill_value
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function unstack(DataFrame $DataFrame, $level, $fill_value): DataFrame
   {
      return $DataFrame;
   }

   /** Modify in place using non-NA values from another DataFrame.
    *
    * @param DataFrame $DataFrame
    * @param           $other
    * @param           $join
    * @param           $overwrite
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function update(DataFrame $DataFrame, $other, $join, $overwrite): DataFrame
   {
      return $DataFrame;
   }

   /** Return unbiased variance over requested axis.
    *
    * @param DataFrame $DataFrame
    * @param           $axis
    * @param           $skipna
    * @param           $level
    * @param           $ddof
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function var(DataFrame $DataFrame, $axis, $skipna, $level, $ddof): DataFrame
   {
      return $DataFrame;
   }

   /** Replace values where the condition is False.
    *
    * @param DataFrame $DataFrame
    * @param           $cond
    * @param           $other
    * @param           $inplace
    * @param           $axis
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function where(DataFrame $DataFrame, $cond, $other, $inplace, $axis): DataFrame
   {
      return $DataFrame;
   }

   /** Return cross-section from the Series/DataFrame.
    *
    * @param DataFrame $DataFrame
    * @param           $key
    * @param           $axis
    * @param           $level
    *
    * @return DataFrame
    * @return DataFrame
    */
   public function xs(DataFrame $DataFrame, $key, $axis, $level): DataFrame
   {
      return $DataFrame;
   }


}